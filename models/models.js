var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    username: {
        type: String,
        required: true
    }
    , password: {
        type: String,
        required: true
    }

});

var prescriptionSchema = new mongoose.Schema({
    text: String,
    username: String,
    createdAt: {type: Date, default: Date.now}
});

mongoose.model("User", userSchema); //declare a schema User, based on userSchema schema
mongoose.model("Prescription", prescriptionSchema);