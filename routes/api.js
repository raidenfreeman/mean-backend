var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Prescription = mongoose.model('Prescription');

router.use(function (req, res, next) {
    if (!(req.method === "GET") && !req.isAuthenticated()) {
        console.log('user not authenticated, redirecting to login');
        res.redirect('/#login');
    }
    else {
        return next(); //TODO: Theloume na min kanei kan get xoris auth
    }
});

router.route('/prescriptions')
//return all prescriptions
    .get(function (req, res) {

        Prescription.find(function (err, prescriptionCollection) {
            if (err) {
                console.log("db error on get");
                return res.send(500, err);
            }

            return res.send(prescriptionCollection);
        });
    })
    .post(function (req, res) {
        var newPrescription = new Prescription();
        newPrescription.text = req.body.text;
        newPrescription.username = req.body.username;
        newPrescription.save(function (err, data) {
            if (err) {
                return res.send(500, err);
            }
            return res.json(data);
        });
    })
    .put(function (req, res) {
        res.send({message: 'TODO put prescriptions'});
    })
    .delete(function (req, res) {
        res.send({message: 'TODO delete prescriptions'});
    });

router.route('/prescriptions/:id')
    .get(function (req, res) {
        res.send({message: 'TODO return prescription with id: ' + req.params.id});
    })
    .post(function (req, res) {
        res.send({message: 'TODO post prescriptions with id:' + req.params.id});
    })
    .put(function (req, res) {
        res.send({message: 'TODO put prescriptions with id:' + req.params.id});
    })
    .delete(function (req, res) {
        res.send({message: 'TODO delete prescriptions with id:' + req.params.id});
    });

module.exports = router;
