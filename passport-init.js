var LocalStrategy = require('passport-local').Strategy;
var bCrypt = require('bcrypt-nodejs');
var mongoose = require('mongoose');
var User = mongoose.model('User');
//temporary data store
module.exports = function (passport) {

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function (user, done) {

        //tell passport which id to use for user
        console.log('serializing user:', user._id);
        return done(null, user._id);
    });

    passport.deserializeUser(function (id, done) {

        //return the user object back
        User.findById(id, function (err, user) {
            if (err) {
                console.log('db error');
                return done(err, false);
            }
            if (!user) {
                return done('user not found', false);
            }

            //user found, return it
            return done(null, user);
        });
    });

    passport.use('login', new LocalStrategy({
            passReqToCallback: true
        },
        function (req, username, password, done) {
            User.findOne({username: username}, function (err, user) {
                if (err) {
                    console.log('db error');
                    return done(err, false);
                }
                if (!user) {
                    console.log('user "' + username + '" doesn\'t exist');
                    return done('<h1>user"' + username + '"doesn\'t exist</h1>', false);
                }
                if (!isValidPassword(user, password)) {
                    return done('invalid password', false);
                }
                console.log('Logged in sucessfully\nHello, ' + username);

                return done(null, user);//return the created object and null errors
            });
        }
    ));

    passport.use('signup', new LocalStrategy({
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) {

            User.findOne({username: username}, function (err, user) {
                if (err) {
                    console.log('db error');
                    return done('<h1>db error</h1>', false);
                }
                if (user) {
                    console.log('user "' + username + '" already exists');
                    return done('username already exists', false);
                }

                var newUser = new User();
                newUser.username = username;
                newUser.password = createHash(password);
                newUser.save(function (err, user) {
                    if (err) {
                        return done(err, false);
                    }
                    console.log('sucessfully signed up user: \t' + username);
                    return done(null, user);
                })
            });
        })
    );

    var isValidPassword = function (user, password) {
        return bCrypt.compareSync(password, user.password);
    };
    // Generates hash using bCrypt
    var createHash = function (password) {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

};